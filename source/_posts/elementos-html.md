---
title: Elementos HTML
date: 2017-02-01 23:45:26
tags:
categories:
---


    {% blockquote David Levithan, Wide Awake %}
    Do not just seek happiness for yourself. Seek happiness for all. Through kindness. Through mercy.
    {% endblockquote %}

{% blockquote David Levithan, Wide Awake %}
Do not just seek happiness for yourself. Seek happiness for all. Through kindness. Through mercy.
{% endblockquote %}

    {% blockquote @DevDocs https://twitter.com/devdocs/status/356095192085962752 %}
    NEW: DevDocs now comes with syntax highlighting. http://devdocs.io
    {% endblockquote %}
    
{% blockquote @DevDocs https://twitter.com/devdocs/status/356095192085962752 %}
NEW: DevDocs now comes with syntax highlighting. http://devdocs.io
{% endblockquote %}


    {% codeblock lang:javascript %}
        alert("Hello World");
        var myVar = "Hello World";
    {% endcodeblock %}

{% codeblock lang:javascript %}
    alert("Hello World");
    var myVar = "Hello World";
{% endcodeblock %}


{% img  /../images/image_0.png 400 %}  


{% link http://www.fabacademy.org [external] FabAcademy %}

{% youtube video_id %}

{% post_path slug %}
{% post_link slug [title] %}

