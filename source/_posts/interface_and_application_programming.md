---
title: Interface and Application Programming
date: 2017-01-12 21:35:36
tags:
categories:
- Programming
- Electronics
---

# Interface and Application Programming
Write an application that interfaces with an [input](http://academy.cba.mit.edu/classes/input_devices/index.html) and/or [output](http://academy.cba.mit.edu/classes/output_devices/index.html) device **that you made**, comparing as many tool options as possible.

## Learning outcomes:
* Interpret and implement design and programming protocols to create a Graphic User Interface (GUI).

## Have you:
* Described your process using words/images/screenshots

* Explained the the GUI that you made and how you did it

* Outlined problems and how you fixed them

* Included original code
