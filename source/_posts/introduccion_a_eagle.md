---
title: Introduccion a Eagle
date: 2017-02-01 00:05:47
tags:
categories:
---

# Diseño de Circuitos - Eagle

![led and button added to hello echo](ledPlusButton.jpg)

## La práctica de esta semana incluye:

*   añadir un botón y un LED a la placa Echo Hello-World,
*   comprobar las normas de diseño,
*   y fabricarla.
*   Revisar la página web de la clase de Neil: [http://academy.cba.mit.edu/classes/electronics_design/index.html](http://academy.cba.mit.edu/classes/electronics_design/index.html)

## Contenido:

*   [Primeros pasos con Eagle](#getting_started_with_eagle)
    *   [Paso 1\. Descargar e instalar Eagle](#download_and_install_eagle)
    *   [Paso 2\. Ejemplo de diagrama esquemático y diseño de placa Echo Hello-World](#step2)
    *   [Paso 3: ¿Qué es un diagrama esquemático?](#step3)
    *   [Paso 4: ¿Qué es el layout de la placa?](#step4)
    *   [Paso 5\. Descarga e instalación de las librerías de componentes](#step5)
    *   [Paso 6: Cómo usar Eagle](#step6)
    *   [Paso 7: Editar el diagrama esquemático](#step7)
    *   [Paso 8: Cómo añadir / conectar componentes en el diagrama esquemático](#step8)
    *   [Paso 9: Cómo trazar las pistas en la placa](#step9)
*   [Trucos y Consejos para Eagle](#tips_tricks)
*   [Exportando un diseño de placa para usar en la Modela](#export_board)
*   [Recursos adicionales para Eagle](#eagle_resources)
    *   [Dónde conseguir los conectores FTDI](#FTDI)
    *   [Enlaces a tutoriales de Eagle](#links)

# <a name="getting_started_with_eagle" id="getting_started_with_eagle"></a>Primeros pasos con Eagle

### <a name="download_and_install_eagle" id="download_and_install_eagle"></a>Paso 1\. Descarga e instalación de Eagle

![eagle logo](eagle.jpg)

### ¿Qué es Eagle?

EAGLE (Easily Applicable Graphical Layout Editor) es una potente aplicación flexible y ergonómica del tipo [EDA](http://en.wikipedia.org/wiki/Electronic_design_automation "Electronic design automation") para diseñar circuitos impresos y realizar esquemas electrónicos con autorouter, es decir con la función que automatiza el dibujo de pistas en la placa de circuitos impresos. Eagle es muy popular entre aficionados debido a su licencia libre y a su gran disponibilidad de bibliotecas de componentes en la web. Utilizaremos Eagle en la práctica de esta semana para diseñar y modificar una placa de circuito impreso.

#### Descarga e instalación

##### Consigue Eagle aquí: [http://www.cadsoftusa.com/download-eagle/](http://www.cadsoftusa.com/download-eagle/)

##### Se puede instalar en Mac, Linux y Windows. Descarga la versión para tu sistema operativo.

*   Ejecuta el instalador de Eagle.
*   Después de instalar la aplicación y abrirla, Eagle te preguntará qué versión quieres instalar - elige "Freeware"

## <a name="step2" id="step2"></a>Paso 2\. Ejemplo de diagrama esquemático y diseño de placa Echo Hello-World

##### Ejemplo de archivos para comenzar

Para ayudarte, puedes descargar los archivos del diagrama esquemático y del diseño de la placa Echo Hello-World. A partir de aquí puedes añadir los componentes necesarios al diagrama del ejemplo:  
[Placa Hello Echo y diagrama esquemático](http://academy.cba.mit.edu/content/tutorials/akf/Downloads/helloEcho.zip)

1.  Vete a Documentos y copia la carpeta extraída en tu carpeta de Eagle.
    *   **Mac**: En tu carpeta "Documentos" > eagle
    *   **Ubuntu:** En tu "home directory" > eagle
    *   **Windows:** En C: > Archivos de programa(x86) > eagle6.4.0 > projects > examples
2.  Abre Eagle
3.  La carpeta helloEcho debería ser visible en "Projects" en el Panel de Control.  
    *   Aseguraté de que los pequeños círculos que aparecen a la derecha de todas las librerías son verdes.  
    *   Si son grises, haz click sobre ellos con el botón derecho del ratón y selecciona "use" para que se transformen en verdes.

Usuarios de Windows - Vuestros archivos aparecerán en una lista bajo "Examples".

![eagle control panel](hello_echo_controlPanel.jpg)

Haz click con el botón derecho del ratón sobre el círculo gris y selecciona "use" en el menú. El círculo pasará a ser verde.

![control panel libaries enabled](control_panel_libaries_enabled.jpg)

## <a name="step3" id="step3"></a>Paso 3: ¿Qué es un diagrama esquemático?

Un diagrama esquemático en diseño de circuitos es un dibujo que representa un circuito, utilizando símbolos para representar los componentes electrónicos reales. El símbolo más básico es un conductor (una pista) que se representa como una línea. Si las líneas se conectan en un diagrama, ésto se representa colocando un punto en la intersección.

Aquí se puede ver el diagrama esquemático de la placa "Hello Echo":

![hello echo schematic](hello_echo_schem.jpg)

### Conceptos de electrónica

*   CORRIENTE ELÉCTRICA es la cantidad de electricidad que recorre un conductor.
*   VOLTAJE es la fuerza que genera la corriente.
*   RESISTENCIA es la oposición a la corriente que presenta un material, componente o circuito.

### Símbolos del Diagrama Esquemático
| Descripcion del componente | Simbolo |
|---|---|
| __resistencia__: Una _resistencia_ es la parte del circuito eléctrico que resiste el flujo de corriente. | ![resistor.png](resistor.png) |
| __condensador__: Un condensador es un dispositivo que se utiliza para almacenar una carga eléctrica y consta de uno o más pares de conductores separados por un aislante. [Los condensadores](http://www.wisegeek.com/what-is-a-capacitor.htm) se utilizan a menudo como dispositivos de filtro para quitar picos de voltaje o de señal en los circuitos electrónicos. | ![cap.png](cap.png) |  
| __LED__: Diodo emisor de luz, un diodo semiconductor que se ilumina cuando se aplica un voltaje. | ![led symbol](led.png)  |
| **Tierra**:  Tierra es el punto de referencia en relación al que se miden otros voltajes en el circuito. Vcc + es positiva con respecto a tierra. Solemos llamar a la tierra "cero voltios", para simplificar las otras mediciones. Coloca el extremo negro (-) de tu multimetro a tierra en un circuito cargado y el extremo rojo (+) del medidor en VCC para medir el voltaje. | ![gnd](gnd.png) |
| **VCC (carga)**:  Vcc+ es positiva con respecto a tierra. | ![VCC](vcc.png) |
| **Conector ISP**  |  ![pin](isp_pin.png) |
| **Microcontrolador**   | ![IC1](attiny_ic.png)  |



## <a name="step4" id="step4"></a>Paso 4: ¿Qué es el Layout de la placa?

El Layout de la placa muestra cómo se organizarán en la placa los componentes electrónicos y las pistas.

Aquí se puede ver el Layout de una placa:

![hello echo board](hello_echo_board.jpg)

## <a name="step5" id="step5"></a>Paso 5: Descarga e instalación de las librerías de componentes

Eagle tiene gran cantidad de librerías que puedes utilizar, pero hay algunas específicas que necesitarás para realizar esta práctica.

### Descarga

##### Descargar librerías:

Puedes descargar todas estas librerías aquí: [http://academy.cba.mit.edu/2012/labs/providence/tutorials/Downloads/eagle_libraries.zip](http://academy.cba.mit.edu/2012/labs/providence/tutorials/Downloads/eagle_libraries.zip)  
<span class="warninglite">  
NOTA: He renombrado las librerías con un 01_ precediendo al nombre de la librería, de modo que puedan aparecer al principio de la lista "Add (Añadir)" al añadir nuevos componentes.</span>

### Instalación de las librerías

##### Cómo instalar las librerías.

Después de descargarlas, descomprímelas y muévelas a la carpeta correspondiente en función del sistema operativo que utilices:

*   En **Mac**, debería estar en /Aplicaciones/EAGLE/lbr/.  
    En **Windows**, estaría en C:\Archivos de programas\EAGLE\lbr\ (fact check this).  
    En **Linux**, la ubicación por defecto de la instalación es /home/eagle-7.2.0/lbr/ (esto es así en la versión 7.2.0, pero puede cambiar en las versionesanteriores)

*   Vete a la parte superior de la barra de herramientas y selecciona el menú "Library".
*   Selecciona "use".

![add libraries](add_libaries.jpg)

Selecciona todas las librerías precedidas con 01_ y selecciona "open".

##### Ahora, cada vez que añadas un componente, todas las librerías estarán disponibles y situadas en lo alto de la lista.

![libraries added](libaries_added.jpg)

### <a name="step6" id="step6"></a>Paso 6: Cómo usar Eagle

#### Eagle Básico

##### Eagle tiene dos ventanas que utilizarás simultáneamente para diseñar una placa:

*   **Schematic (.sch)** - componentes lógicos
*   **Board Layout (.brd)** para la placa que vamos a fresar
*   Existe un botón schematic / board para que puedas cambiar entre ambas ventanas.![libraries added](eagle_switch_brd_sch.png)

*   No cierres la ventana 'layout' ni 'schematic'; necesitas mantener ambas abiertas constantemente.
*   Si cierras una ventana mientras la otra permanece abierta (y realizas cambios sobre ella) se romperá el vínculo entre las dos.
*   Si la cierras, no abras uno de los archivos; Eagle te advertirá:![libraries added](eagle_annotation_severed.png)

#### Trabajando con la interfaz de Eagle

Existen dos formas de interactuar con Eagle:

*   **Barra de iconos gráficos:** Puedes usar los iconos de la barra izquierda, colocando el ratón sobre ellos para ver lo que hacen (habilitado por defecto).
    *   La lista de comandos de la línea de comandos que se incluyen a continuación también se refiere a los iconos.
    *   ERC y DRC se encuentran en "tools" en el menú de la parte superior.

*   **Línea de Comandos**: Puedes empezar a introducir por teclado un comando en cualquier momento y seleccionar el elemento con el que quieras interactuar en las ventanas 'schematic' o 'board'. (Ver la sección de comandos que se incluye a continuación).

##### Eagle tiene una línea de comandos (tan sólo tienes que comenzar a escribir un comando) - los comandos básicos incluyen:

*   **add** = abre las librerías para que puedas añadir componentes en la ventana 'schematic'.
    *   elige siempre los componentes 1206
    *   son los únicos con paquetes de 12mm x 6mm
*   **move** = mueve un elemento
*   **net** = realiza una conexión lógica
*   **junction** = añade una intersección
*   **value** = añade un valor a un componente (por ejemplo, un valor en ohmios)
*   **name** = nombra un componente
*   **label** = etiqueta el nombre de un componente en la ventana 'schematic'.
*   **copy** = copia un componente existente en la ventana 'schematic'.
    *   renombrar los componentes que copias
*   **route** = se utiliza en la ventana 'Layout' para indicar si necesitas añadir una conexión (sigue las líneas amarillas)
*   **ERC** = comprueba las normas de electrónica; te asegura que tu placa funcionará (se utiliza en la ventana 'schematic')
*   **DRC** = comprueba las normas de diseño (en ventana 'board') - manten todos los parámetros por defecto (16 mil. es correcto); debería mostrar el mensaje "no error" en la parte inferior izquierda de la pantalla
*   **group** = agrupa componentes en la ventana 'layout'; si marcas con el botón derecho del ratón, puedes elegir _Move: Group_ para mover el grupo
*   **rats** = en la ventana 'board', indica si tienes airwires
*   **rip** = borra las conexiones en la ventana 'board'
*   **show** = cuando escribes esta orden, selecciona un componente y muestra información sobre éste en la parte inferior izquierda de la pantalla. Además, si escribes show + [nombre del componente] podrás ver que dicho componente aparece resaltado. Puedes utilizar esta orden para ver todas las pistas que van a tierra, por ejemplo.
*   **text** = permite añadir texto a tu placa También puedes editar el archivo .png que exportes en Gimp para introducir textos e imágenes en blanco y negro. (Recomiendo añadir texto en Gimp).
*   **info** = marca un texto para obtener propiedades del texto

## <a name="step7" id="step7"></a>Paso 7: Editar el diagrama esquemático

### Qué hacer (Resumen)

##### Primero editamos el diagrama esquemático.

Más sobre cómo añadir componentes y trabajar con Eagle más abajo, en la próxima sección.

1.  Añadiremos componentes en la ventana 'schematic' y crearemos conexiones entre ellos.
2.  A continuación, verificaremos el diseño usando el ERC (Electrical Rules Check)
3.  Después de conectar los componentes y arreglar posibles errores, cambiaremos a la ventana 'layout' para colocar los componentes y trazar las pistas.

##### Partes que aparecen en la placa Hello Echo

*   **conector de 6 pines:** para programar la placa
*   **microcontrolador:** attiny44A. Una vez programado el microcontrolador, el programa se almacena en la memoria no volátil. Esto significa que recordará el programa.
*   **conector FTDI:** carga la placa y permite que ésta se comunique con el ordenador
*   **resonador de 20MHz:** reloj externo. El attiny tiene un reloj de 8Mhz pero el resonador es más rápido (aumenta la velocidad del reloj del procesador) y más preciso.

##### Necesitarás añadir los siguientes componentes al diagrama esquemático:

*   **Resistencia** (valor de 10k)
    *   Finalidad: resistencia pull-up.
    *   ¿Qué es una resistencia pull-up?  
        Ver: [http://www.sparkfun.com/tutorials/218](http://www.sparkfun.com/tutorials/218)
*   **Botón** (OMERON switch)
*   **Tierra**
*   **VCC**
*   **Conectar el pin 10** (PA3) del microcontrolador con el botón
*   **LED** (Diodo emisor de luz) - Los LEDs tienen polaridad - el lado de la línea es el cátodo y se conecta al lado de la tierra.(Ver diagrama esquemático abajo)
*   **Resistencia** (valor de 499 ohms)
    *   Finalidad: resistencia de limitación de corriente
    *   ¿Para qué necesitamos una resistencia de limitación de corriente? Para no quemar el LED.
    *   Los LEDs que estamos usando están preparados para un voltaje de 1.8V
    *   La corriente máxima que puede soportar el LED es:  
        Ver: [http://led.linear1.org/1led.wiz](http://led.linear1.org/1led.wiz)
    *   Mirar la ficha técnica del LED para obtener los valores para conectarlo: [Ficha Técnica](http://academy.cba.mit.edu/content/tutorials/akf/Datasheets/LTST-C150CKT.pdf)

![Led_calculator](led_calculator.jpg)

*   Tenemos que usar una resistencia de 82 ohm o superior para evitar romper el LED He elegido una resistencia de 499 ohm pero se podría usar una de 100 y también funcionaría.

##### Ejemplo de un diagrama esquemático con LED y botón incorporados con las resistencias y conexiones de alimentación y tierra apropiadas.

![example schematic with button and led](schematic_updated.png)

### <a name="step8" id="step8"></a>Paso 8: Cómo añadir y conectar componentes en el diagrama esquemático

#### Cómo añadir los componentes

Añade un componente (Escribe "add" selecciona el icono "add" de la barra de herramientas. El menú añadir se abrirá.

Puedes buscar en la lista un componente para añadirlo o puedes escribir su nombre en la casilla "search".

##### Añade un LED (en la librería FAB_Hello)

![add LED](add_LED.jpg)

##### Añade una resistencia - resistor - (Necesitarás dos - en la librería ng)

![add resistor](add_resistor.jpg)

Haz click en "0k" y luego haz click en el diagrama esquemático para colocar el componente.

##### Añade un botón - button - (en la librería fab)

![add button](add_switch_fab.jpg)

Haz click en "0k" y luego haz click en el diagrama esquemático para colocar el componente.

##### Añade la tierra - Ground -

![add gnd](add_gnd_updated.jpg)

Haz click en "0k" y luego haz click en el diagrama esquemático para colocar el componente.

##### Añade VCC (conexión a la alimentación)

![add vcc](add_vcc_fabhello.jpg)

Haz click en "0k" y luego haz click en el diagrama esquemático para colocar el componente.

#### Cómo conectar los componentes

##### Hay dos formas de conectar los componentes en un diagrama esquemático:

1.  Puedes conectar los componentes con un cable (también llamado "net" en Eagle). Esto puede generar conexiones de manera aparentemente sencilla, pero que pueden llegar a complicarse rápidamente ya que las nets se cruzan unas con otras.  

2.  También puedes <span class="warninglite">nombrar las nets</span> asociadas a los componentes que necesitan estar conectados usando el mismo nombre. Ver el ejemplo del diagrama esquemático incluído anteriormente. Eagle te preguntará si quieres que estén conectados. (¡Di que sí!). <span class="warninglite">Después de nombrar el componente - etiquetaló para que aparezca en el diagrama.</span>  

    Por ejemplo:
    *   Todos los componentes del diagrama esquemático llamados GND están conectados a un punto de tierra común.
    *   el botón está conectado con una net que enlaza el botón con un pin del microcontrolador.  

3.  <span class="warninglite">Usa el pin 10 para conectar con el botón.</span>
4.  <span class="warninglite">Usa el pin 6 para conectar con el LED.</span>  

![diagram](circled_buttonled_diagram_small.png)  

*   A continuación, conectar dos componentes, NET -- NAME -- LABEL (necesitarás una net para ambos, el componente y el pin del microcontrolador)

## <a name="step9" id="step"></a>Paso 9: Cómo trazar las pistas en la placa

*   Cambia a la ventana 'board'
    *   Vete al menu superior > File > switch to board
    *   La ventana 'board' se abrirá
    *   Los componentes que has añadido aparecerán en una esquina unidos a la placa con líneas amarillas.

![diagram](eagle_board_unrouted.png)

*   Usa "move" para mover cada componente individual.
*   Usa "route" para trazar cada pista. La línea se convertirá en roja a medida que traces.
*   Después de trazar las líneas, puedes usar la herramienta "move" para moverlas como líneas.
*   Traza todas las pistas hasta que obtengas algo parecido al diagrama que aparece a continuación.
*   También existe la opción autorouter - es útil para hacer un esquema inicial, pero probablemente tendrás que editar las pistas manualmente, especialmente en este circuito.
    *   para usar la opción autorouter vete al menú "tools" en la barra superior y selecciona "auto"
    *   también puedes escribir "auto"
*   Tu placa no tiene que ser exactamente igual que ésta; hay diferentes modos de trazar las pistas. Tan sólo emplea ésta como referencia.

![routed traces](buttonled_board_small.png)

## <a name="tips_tricks" id="tips_tricks"></a>Trucos y consejos para Eagle

*   En la versión gratuita de Eagle, no coloques nada debajo o a la izquierda de la cruz (origen) en ninguna de las dos ventanas (schematic o board).
*   Truco de la resistencia 0 ohm: coloca una resistencia de 0 ohm sobre una pista para saltar una pista existente.
*   junction (el símbolo es un punto) puede ser útil si se rompe una net en dos partes - indica si dos nets que se cruzan están conectadas o no.
*   en options puedes cambiar el color de fondo de las ventanas schematic y board (User Interfaces)
*   haz click en display para ver las capas que puedes mostrar/ocultar
*   organiza los componentes en la ventana 'board' - utiliza el botón derecho para girar componentes

# <a name="export_board" id="export_board"></a>Exportando un diseño para fresar en la Modela

##### Preparar la placa para fresar:

En el menú layers de la barra de herramientas superior, selecciona únicamente la capa superior (top layer)  

![traces only](display_hide_layers_board_file_export.jpg)

*   a continuación, expórtalo como un archivo png (file -- export -- image)
*   marca la casilla MONOCHROME y resolucion 500 dpi - esto exportará una imagen con pistas de color blanco.
*   asegúrate de que el modo de imagen es grayscale: image > mode > grayscale
*   a continuación exporta Dimension layer para fresar la parte exterior de la placa
*   Recuerda: la Modela retira la parte de color negro y deja la parte de color blanco.

#### PNG exportado

![diagram](traces_export_file.png)

Puedes editar tu archivo .png en Gimp. El texto añadido en Gimp puede cambiarse a negrita (más fuerte y más grueso) y así es menos probable que se desprenda al fresar la placa.

#### Editar archivos / Crear ficheros de fresado y corte.

*   Abrir el archivo .png de la placa en Gimp
*   Recortar el archivo si es necesario
*   Ir a "Modify" menú > Canvas > Canvas Size. En la versión 2.8 de Gimp sería Image> Canvas Size

![imags_canvassize](images_canvassize.jpg)

![add 20 px](add_20px_to_20canvas_size.jpg)

#### Añadir un contorno blanco

##### Crear el fichero pistas.png

1.  Añadir 20 px de ancho (aseguraté de que las proporciones están vinculadas).
2.  Tras añadir 20px, marca en el botón "center" para centrar la imagen o redimensionar el lienzo de imagen.
3.  Aplana la imagen - ir al menu "Image" > flatten image
4.  Ahora tendrás un contorno de color blanco de 20px rodeando el fichero de pistas de la placa.
5.  Guarda el archivo como pistas.png. Este es el archivo que usarás para fresar las pistas en la Modela.
6.  Puedes añadir texto de color blanco o imágenes para fresar junto con las pistas.

**Ejemplo de fichero pistas.png**

Está imagen no esta a escala, ¡a propósito! Tienes que crear tu propio archivo. Si fresas las imágenes que aparecen a continuación, los tamaños serán incorrectos.

![finished traces](HelloFTDI_FixedButtonPin_traces.png)

##### Crear el fichero exterior.png

1.  Comenzar con el fichero pistas.png que acabas de crear.
2.  Guardar el archivo con un nombre diferente (exterior.png).
3.  Usar el bote "bucket" / herramienta para rellenar todos los espacios de color blanco EXCEPTO el CONTORNO. Deja el contorno blanco alrededor de la imagen. Éste le usaremos para crear el archivo exterior para cortar la placa. Usar la imagen de las pistas para comenzar garantiza que tus archivos están bien centrados y que no cortaras sobre las pistas.
4.  Guarda el archivo. Este es el archivo que usarás para cortar el exterior de la placa en la Modela.

**Ejemplo de exterior.png**

Está imagen no esta a escala, ¡a propósito! Tienes que crear tu propio archivo. Si fresas las imágenes que aparecen a continuación, los tamaños serán incorrectos.  
![finished interior](HelloFTDI_FixedButtonPin_interior.png)

# <a name="eagle_resources" id="eagle_resources"></a>Recursos adicionales para Eagle

## <a name="FTDI" id="FTDI"></a>Dónde conseguir los conectores FTDI

##### Para encontrar el conector FTDI:

*   Necesitas la librería SparkFun Eagle - ver la sección librerías más arriba.
*   Haz click en "add" (símbolo de la barra de herramientas o línea de comandos).
*   Busca "Header 6"
*   Haz click en "OK"
*   Coloca el componente.  

![add_1_06_SMD](add_1x06_SMD.jpg)  

##### 1X06-SMD en la ventana 'board':

##### ![SMD board](ftdi_pads.jpg)  

FTDI Through Hole:  

Puedes usar el componente through-hole (no necesitas perforar los agujeros) y funcionará el conector smd.

![longpads](long_pads.jpg)  

##### 1X06-LONGPADS en la ventana 'board':

![SMD board](longpads_board.jpg)

## <a name="links" id="links"></a>Enlaces a tutoriales de Eagle (información extra - no se requiere en la práctica)  

[Tutorial CadSoft's para Eagle v6 [PDF] (inglés)](http://academy.cba.mit.edu/content/tutorials/akf/Downloads/V6_tutorial_en.pdf)

[Introducción al Panel de Control de Eagle de CadSoft's [PDF] (inglés)](http://academy.cba.mit.edu/content/tutorials/akf/Downloads/TUTORIAL-part1_Introduction-of-the-Control-Panel.pdf)

[Crear un proyecto en CadSoft's / Comenzar a dibujar diagramas esquemáticos [PDF] (inglés)](http://academy.cba.mit.edu/content/tutorials/akf/Downloads/TUTORIAL-part2Create-a-project-and-start-drawing-schematics.pdf)[  
](http://fab.cba.mit.edu/content/tools/circuits/pcb_design/pcb_design.html)

[Tutorial para principiantes (inglés)](http://khammami.blogspot.com/2008/10/how-to-use-eagle-for-beginner.html)[  
](http://fab.cba.mit.edu/content/tools/circuits/pcb_design/pcb_design.html)

[Crear una librería personalizada (inglés)](http://www.instructables.com/id/How-to-make-a-custom-library-part-in-Eagle-CAD-too/)[  
](http://fab.cba.mit.edu/content/tools/circuits/pcb_design/pcb_design.html)
