---
title: Output Devices
date: 2017-01-17 21:35:36
tags:
categories:
- Electronics
---

# Output Devices
Add an output device to a microcontroller board you've designed and program it to do something

## Learning outcomes:
* Demonstrate workflows used in circuit board design and fabrication

* Implement and interpret programming protocols

## Have you:
* Described your design and fabrication process using words/images/screenshots.  

* Explained the programming process/es you used and how the microcontroller datasheet helped you.

* Outlined problems and how you fixed them

* Included original design files and code
